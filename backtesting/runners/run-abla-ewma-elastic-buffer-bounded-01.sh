#!/bin/bash
bname="abla-ewma-elastic-buffer-bounded-01-blocksizes-bch"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../datasets/blocksizes-bch.csv" 707107 353553 0 37938 192 37938 10 707107 303514 707107 151757 3
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:16000000]" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-blocksizes-btc"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../datasets/blocksizes-btc.csv" 707107 353553 0 37938 192 37938 10 707107 303514 707107 151757 3
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:16000000]" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-chunksizes-eth"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../datasets/chunksizes-eth.csv" 2378414 353553 0 37938 192 37938 10 2378414 303514 8000000 151757 3
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:16000000]" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-chunksizes-ltc"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../datasets/chunksizes-ltc.csv" 1000000 500000 0 37938 192 37938 10 1000000 303514 1000000 151757 3
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:16000000]" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-hybrid-btc+ltc+eth+bch"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../datasets/hybrid-btc+ltc+eth+bch.csv" 707107 353553 0 37938 192 37938 10 707107 303514 707107 151757 3
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:16000000]" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-hypothetical-eth-x64"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../datasets/hypothetical-eth-x64.csv" 32000000 16000000 0 37938 192 37938 10 32000000 303514 128000000 151757 3
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:1024000000]" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-hypothetical-hybrid-x64"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../datasets/hypothetical-hybrid-x64.csv" 32000000 16000000 0 37938 192 37938 10 32000000 303514 128000000 151757 3
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:1024000000]" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-ne-blocksizes-bch"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../datasets/blocksizes-bch.csv" 707107 353553 0 37938 192 37938 10 707107 303514 707107 151757 0
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:16000000]" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-ne-blocksizes-btc"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../datasets/blocksizes-btc.csv" 707107 353553 0 37938 192 37938 10 707107 303514 707107 151757 0
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:16000000]" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-ne-chunksizes-eth"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../datasets/chunksizes-eth.csv" 2378414 353553 0 37938 192 37938 10 2378414 303514 8000000 151757 0
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:16000000]" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-ne-chunksizes-ltc"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../datasets/chunksizes-ltc.csv" 1000000 500000 0 37938 192 37938 10 1000000 303514 1000000 151757 0
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:16000000]" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-ne-hybrid-btc+ltc+eth+bch"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../datasets/hybrid-btc+ltc+eth+bch.csv" 707107 353553 0 37938 192 37938 10 707107 303514 707107 151757 0
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:16000000]" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-ne-hypothetical-eth-x64"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../datasets/hypothetical-eth-x64.csv" 32000000 16000000 0 37938 192 37938 10 32000000 303514 128000000 151757 0
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:1024000000]" plot-abla-ewma-elastic-buffer-bounded.gp
) &

bname="abla-ewma-elastic-buffer-bounded-01-ne-hypothetical-hybrid-x64"
title="$bname"
(./run-abla-ewma-elastic-buffer-bounded.sh "../results/$bname.csv" "../datasets/hypothetical-hybrid-x64.csv" 32000000 16000000 0 37938 192 37938 10 32000000 303514 128000000 151757 0
gnuplot -e "infile='../results/$bname.csv'; set output '../results/$bname.png'; set title '$title'; set autoscale yfix; set yrange [0:1024000000]" plot-abla-ewma-elastic-buffer-bounded.gp
) &
