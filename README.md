# CHIP-2023-04 Adaptive Blocksize Limit Algorithm for Bitcoin Cash

        Title: CHIP-2023-04 Adaptive Blocksize Limit Algorithm for Bitcoin Cash
        First Submission Date: 2023-04-13
        Owners: bitcoincashautist (ac-A60AB5450353F40E)
        Type: Technical, automation
        Layers: Network, consensus-sensitive
        Status: Proposed
        Last Edit Date: 2023-10-16

## Contents

<details><summary><strong>Table of Contents</strong></summary>

- [Summary](#summary)
- [Deployment](#deployment)
- [Motivation](#motivation)
- [Benefits](#benefits)
- [Technical Description](#technical-description)
- [Specification](#specification)
- [Security Analysis](#security-analysis)
- [Implementations](#implementations)
- [Test Cases](#test-cases)
- [Activation Costs](#activation-costs)
- [Ongoing Costs](#ongoing-costs)
- [Risk Assessment](#risk-assessment)
- [Evaluation of Alternatives](#evaluation-of-alternatives)
- [Discussions](#discussions)
- [Statements](#statements)
- [Acknowledgements](#acknowledgements)
- [Changelog](#changelog)
- [Copyright](#copyright)

</details>

## Summary

[[Back to Contents](#contents)]

Needing to coordinate manual increases to Bitcoin Cash's blocksize limit incurs a meta cost on all network participants.

The need to regularly come to agreement makes the network vulnerable to social attacks.

To reduce Bitcoin Cash's social attack surface and save on meta costs for all network participants, we propose an algorithm for automatically adjusting the blocksize limit after each block, based on the exponentially weighted moving average size of previous blocks.

This algorithm will have minimal to no impact on the game theory and incentives that Bitcoin Cash has today.
The algorithm will preserve the current 32 MB limit as the floor "stand-by" value, and any increase by the algorithm can be thought of as a bonus on top of that, sustained by actual transaction load.

This algorithm's purpose is to change the default response in the case of mined blocks increasing in size.
The current default is "do nothing until something is decided", and the new default will be "adjust according to this algorithm" (until something else is decided, if need be).

If there is ever a need to adjust the floor value, algorithm's parameters, or remove the algorithm, that can be done with the same amount of work that would have been required to change the blocksize limit.

## Deployment

[[Back to Contents](#contents)]

Deployment of this specification is proposed for the May 2024 upgrade.

## Motivation

[[Back to Contents](#contents)]

**Why a limit at all?**

Bitcoin Cash's blocksize limit serves several purposes.
Perhaps most critically, without it, individual participants would still have their own maximum acceptable blocksizes depending on their hardware and software stack, and any difference in that configuration could lead to a chain split.

It also works as a Denial of Service preventative measure.
A sufficiently large block will bring down any service that processes BCH blocks in the back end, so an attacker with enough resources to mine a single block could deny users access to a large number of services.
It is important to note that these services often have their bottlenecks outside of the full node software.

The limit works as spam protection by limiting the number of low economic value transactions an attacker can force the network to process.
For a practical example, consider BSV: it is prohibitively expensive for service providers like block explorers and exchanges to support BSV not just because the blocks are big, but because comparatively few customers are actually paying for the services made possible by processing and storing those transactions.
The solution for those businesses is to outsource their nodes, and the result is a network with extremely few nodes that is neither robust nor censorship-resistant.

The flipside of the spam protection is that the blocksize limit can be thought of as a minimum technical requirement of network infrastructure.
Theoretically, the network could start consistently producing blocks at the limit at any time (whether or not the transactions are economically relevant), and so the limit tells maintainers of network infrastructure exactly what the scaling expectations are.

<details><summary><strong>Brief history of the blocksize limit</strong></summary>

This limit was [first set to 1MB in 2010, by Satoshi Nakamoto](https://github.com/bitcoin/bitcoin/commit/8c9479c6bbbc38b897dc97de9d04e4d5a5a36730#diff-608d8de3fba954c50110b6d7386988f27295de845e9d7174e40095ba5efcf1bbR1422) himself.
It was activated with block 79,401 (2010-09-12), which was the first block to have the limit enforced.
For a long time the limit served its anti-DoS purpose without affecting utility of the network, and then, some time in 2015, Bitcoin adoption caught up and the limit became inadequate, which the chart below clearly shows.

<p align="center"><img src="img/fig1-historical-limit.png"></img></p>

The daily frequency of blocks over 90% full was still <10% until January 2015 (blockchain day 2364).
From then onward, over 90% full blocks were quickly becoming more and more frequent.
Despite this, efforts to coordinate an increase of the limit were met with failure, and the problem only became worse over time resulting in most blocks becoming over 90% full and users having to aggressively outbid each other in order to transact.

The frustration with this situation resulted in a subset of Bitcoin network deciding to change the limit, which resulted in a permanent network split (a hard fork), and the bigger limit network is now known as Bitcoin Cash.
The timeline of the Bitcoin scaling debate is [documented here](https://hackernoon.com/the-great-bitcoin-scaling-debate-a-timeline-6108081dbada).

The Bitcoin Cash network increased the limit two times:

- to 8MB, as part of [2017 user-activated hard fork (UAHF)](https://documentation.cash/protocol/forks/bch-uahf), with block [478,559 (2017-08-01)](https://explorer.bitcoinunlimited.info/block/000000000000000000651ef99cb9fcbe0dadde1d424bd9f15ff20136191a5eec) creating the split by being the first one accepted having size >1MB and &le;8MB (1,915,175 bytes);
- to 32MB, as part of [May 2018 consensus specification upgrade](https://documentation.cash/protocol/forks/hf-20180515), with block [530,355 (2018-05-15)](https://explorer.bitcoinunlimited.info/block-height/530355) being the first one which would have been accepted having size >8MB and &le;32MB. Note that the first block with size >8MB actually came much later: block [546,053 (2018-09-02)](https://explorer.bitcoinunlimited.info/block-height/546053) was the one which first successfully tested the new limit with size of 8,854,882 bytes.

The first increase was very turbulent, because it split one network into two and the market went through a volatile process of discovery of what value to assign to each.
The second increase was smooth, because network participants could all agree to further lift it.
Even when done in full agreement, it required a coordinated effort to avoid risks of accidental splits: the exact moment and amount of increase was agreed upon and specified ahead of time, and the increase executed using the MTP activation mechanism.

</details>

**Why an algorithmic blocksize limit?**

Even when done in full agreement, increases to the blocksize limit require a coordinated effort to avoid risks of accidental splits: the exact moment and amount of increase must be agreed upon and specified ahead of time.
That coordination effort incurs a non-zero cost on network participants.
The more the network grows, the greater the meta cost will be.
And of course, very few things are ever done in "full agreement", even without social attacks intentionally dividing the community.
The more the network grows, the greater the risk of network stakeholders entering a deadlock will be.

The Bitcoin Cash network has had success arriving at reasonable processes for coming to decisions as a network, and it has become more resilient over the years.
Still, the network participants have limited resources to devote to coming to agreement, let alone defending against social attacks.
An algorithmic blocksize limit makes it so that the default response to larger mined blocks is a larger blocksize limit, and puts the burden of "doing something" on whoever wants to deviate from that.

## Benefits

[[Back to Contents](#contents)]

The proposed block size adjustment algorithm retains all of the benefits laid out in the "Why a limit at all?" section above, and removes the need for coordinated changes to the blocksize in the future, reducing meta costs for the network significantly.

By using mined block sizes as the input for the algorithm, we ensure that the limit is influenced by the demand from users (in the form of transactions) only as much as the aggregate mining network is willing to supply blockspace.
By implementing the current 32 MB limit as a floor value for the algorithm, we will still maintain a level of "stand-by" capacity even when the network is underused.
This prevents the algorithm from introducing any changes to the incentive system.

The exponential nature of the algorithm makes it mathematically elegant, and it [exhibits no bad behavior at edge cases](#algorithm-too-fast).
By implementing asymmetry in the per-block response, [we ensure stability](#spam-attack) even under extreme conditions.
The change in the blocksize limit will always be predictable and smooth, across the full range of possible scenarios.

By implementing an elastic buffer in the algorithm, we [ensure that bursts of demand can be accommodated](#algorithm-too-slow) by responsive increases in the blocksize limit.

Finally, implementing an algorithmic limit will signal Bitcoin Cash’s commitment to scaling the network to meet demand.

## Technical Description

[[Back to Contents](#contents)]

The main function driving the proposed algorithm belongs to the exponentially weighted moving average (EWMA) family.
We will refer to this main function as the **"control function"**.
The output of the control function, referred to as **"control block size"**, is incremented by an **"elastic buffer size"** value to obtain the block size limit.
The input to the control function is scaled by a constant **"asymmetry factor"**, and by a variable scaling factor determined by the elastic buffer size -- in case of positive adjustment response.

The elastic buffer size is determined by the secondary **"elastic buffer function"**, which is driven by rate of change of the control function.
If the rate of change of control function is positive and big enough, then the elastic buffer size will grow to provide more headroom than the control function alone would.
Unlike the control function, the buffer won't be reduced in response to smaller block sizes but it will decay with time -- for a while preserving "memory" of a past increase.
If positive rate of change of control function is below a threshold value, then exponential decay will take over and work to reduce the buffer size.
The buffer size to control block size ratio has an upper bound in case it grows enough so that exponential decay arrives at equilibrium with the maximum rate of increase.

The full algorithm is defined as follows:

- y<sub>n</sub> = &epsilon;<sub>n</sub> + &beta;<sub>n</sub> ;
- &epsilon;<sub>n</sub> = &epsilon;<sub>0</sub> , if n &leq; n<sub>0</sub> ;
- &epsilon;<sub>n</sub> = &epsilon;<sub>n-1</sub> + &gamma; &sdot; (&zeta; &sdot; x<sub>n-1</sub> - &epsilon;<sub>n-1</sub> - &zeta; &sdot; &beta;<sub>n-1</sub> &sdot; (&zeta; &sdot; x<sub>n-1</sub> - &epsilon;<sub>n-1</sub>) / (&zeta; &sdot; y<sub>n-1</sub> - &epsilon;<sub>n-1</sub>)) , if n > n<sub>0</sub> and &zeta; &sdot; x<sub>n-1</sub> > &epsilon;<sub>n-1</sub> ;
- &epsilon;<sub>n</sub> = max(&epsilon;<sub>n-1</sub> + &gamma; &sdot; (&zeta; &sdot; x<sub>n-1</sub> - &epsilon;<sub>n-1</sub>), &epsilon;<sub>0</sub>) , if n > n<sub>0</sub> and &zeta; &sdot; x<sub>n-1</sub> &leq; &epsilon;<sub>n-1</sub> ;
- &beta;<sub>n</sub> = &beta;<sub>0</sub> , if n &leq; n<sub>0</sub> ;
- &beta;<sub>n</sub> = max(&beta;<sub>n-1</sub> - &theta; &sdot; &beta;<sub>n-1</sub> + &delta; &sdot; (&epsilon;<sub>n</sub> - &epsilon;<sub>n-1</sub>), &beta;<sub>0</sub>) , if n > n<sub>0</sub> and &zeta; &sdot; x<sub>n-1</sub> > &epsilon;<sub>n-1</sub> ;
- &beta;<sub>n</sub> = max(&beta;<sub>n-1</sub> - &theta; &sdot; &beta;<sub>n-1</sub>, &beta;<sub>0</sub>) , if n > n<sub>0</sub> and &zeta; &sdot; x<sub>n-1</sub> &leq; &epsilon;<sub>n-1</sub> .

where:

- y stands for block size limit;
- n stands for block height;
- &epsilon; (epsilon) is the "control function";
- &beta; (beta) is the "elastic buffer function";
- n<sub>0</sub>, &epsilon;<sub>0</sub>, and &beta;<sub>0</sub> are initialization values;
- x stands for mined block size;
- &gamma; (gamma) is the control function's "forget factor".
This controls the maximum per-block rate of the adjustment.
The greater the value the greater the response to a single block's size;
- &zeta; (zeta) is the control function's "asymmetry factor".
This controls the difference between maximum increase and decrease rates;
- &theta; (theta) is the elastic buffer decay rate.
With this, the buffer will be reduced with each step when geared growth rate of the control function is less than the decay rate;
- &delta; (delta) is the elastic buffer "gearing ratio".
With this, growth rate of the elastic buffer is geared to growth rate of the control function.

### Configuration Constants

#### Mainnet

Proposed constants for **mainnet** are:

- &epsilon;<sub>0</sub> = 16000000 ,
- &beta;<sub>0</sub> = 16000000 ,
- n<sub>0</sub> = n<sub>upgrade10</sub> ,
- &zeta; = 1.5 ,
- &gamma; = 1 / 37938 ,
- &delta; = 10 , and
- &theta; = 1 / 37938 .

This means that initial block size limit (&epsilon;<sub>0</sub> + &beta;<sub>0</sub>) will match the current limit of 32000000, which will also be the floor block size limit value for the algorithm.

The value of n<sub>upgrade10</sub> is defined as the height of first block with median time past (MTP) greater than upgrade 10 activation time (tentative Unix time of 1715774400).

#### Testnets

- testnet3 and testnet4: algorithm disabled, still to use flat limit
- scalenet and chipnet: use the algorithm with same &zeta;, &gamma;, &delta; and &theta; configuration, but with activation MTP and different &epsilon;<sub>0</sub> and &beta;<sub>0</sub> values:
	+ chipnet: &epsilon;<sub>0</sub> = &beta;<sub>0</sub> = 1000000 ,
	+ scalenet: &epsilon;<sub>0</sub> = &beta;<sub>0</sub> = 128000000 .

### Implementation Notes

#### Arithmetic Overflow Safety

The [reference implementation (C++)](#reference-implementation-c-1) specifies the exact method of calculating the above using integer arithmetics, and executes additional steps before and after carrying out the above defined calculation.

Normally, a block size value greater than the limit can not be passed as input to the algorithm.
However, for implementation safety, the input is first clamped to the limit:

- x<sub>n-1</sub> = min(x<sub>n-1</sub>, y<sub>n-1</sub>).

Then, the new algorithm state (&epsilon;<sub>n</sub>, &beta;<sub>n</sub>) is calculated using the clamped input.

In addition to the above, and as a last step in calculation, the reference implementation clips the calculated values with:

- &epsilon;<sub>max</sub> = 2837960626724546304 ,
- &beta;<sub>max</sub> = 9459868755748488064 ,
- &epsilon;<sub>n</sub> = min(&epsilon;<sub>n</sub>, &epsilon;<sub>max</sub>) ,
- &beta;<sub>n</sub> = min(&beta;<sub>n</sub>, &beta;<sub>max</sub>) .

This guarantees correctness across the full uint64 range, and ensures that:

- the algorithm can not exceed maximum adjustment even if fed inputs out of bounds,
- the limit can not exceed the maximum uint64 value,
- the algorithm can not enter a state in which internal calculations could overflow uint64.

Values &epsilon;<sub>max</sub> and &beta;<sub>max</sub> limits are configuration-specific and the above values have been calculated to match the above proposed configuration parameters.

#### 32-bit Implementation Limitation

Due to limitations in the P2P protocol (as well as the block data file format), and for correctness sake, we must specify a temporary post-filter limit at 2 GB:

- y<sub>temporary_max</sub> = 2000000000 ,
- y<sub>n</sub> = min(y<sub>n</sub>, y<sub>temporary_max</sub>) .

This should be removed on or before May 2028, which is well before the algorithm having a chance to reach 2 GB as it would take 4.8 years to reach 2 GB even with 100% block full 100% of the time.

For the limit to be removed, 2 things will need to be done:

1. No longer support 32-bit architectures in BCH
2. Fix the p2p protocol to allow for p2p messages larger than `uint32_t`

### Mathematical Characteristics

Extreme rates of change of the control curve are determined from the constants, and given by:

- ((&epsilon;<sub>n</sub> - &epsilon;<sub>n-1</sub>) / &epsilon;<sub>n-1</sub>)<sub>max</sub> = &gamma; &sdot; (&zeta; - 1) = 1 / 75876
- ((&epsilon;<sub>n</sub> - &epsilon;<sub>n-1</sub>) / &epsilon;<sub>n-1</sub>)<sub>min</sub> = -&gamma; = -1 / 37938

These rates compound to theoretical year-over-year maximum of +200% and minimum of -75%.

Extreme buffer size relative to control block size is determined from the constants, and given by:

- (&beta;<sub>n</sub> / &epsilon;<sub>n</sub>)<sub>max</sub> = &delta; &sdot; &gamma; / &theta; &sdot; (&zeta; - 1) / (&gamma; / &theta; &sdot; (&zeta; - 1) + 1)   = 3.33

Buffer decay rate compounds to -75% year-over-year, and half-life is given by:
- log(0.5) / log(1 - &theta;) = 26296,
which is 183 days with average block time of 10 minutes.

Relationship between all the parts of the control function is illustrated by the figure below, and given here for reference.
We will discuss the parts in the sections that follow.

**Figure:** step rate of change (r<sub>n-1</sub> = (&epsilon;<sub>n</sub> - &epsilon;<sub>n-1</sub>) / &epsilon;<sub>n-1</sub>) of control function, as a function of mined block size (x<sub>n-1</sub>).

<p align="center"><img src="img/fig-cfresponse.png" width=400></img></p>

#### Control Function

The EWMA family of functions has proven itself in many [control engineering](https://en.wikipedia.org/wiki/Control_engineering) applications across various industries:

- It is very similar to the first order IIR filter, described in [Arzi, J., "Tutorial on a very simple yet useful filter: the first order IIR filter", section 1.2](http://www.tsdconseil.fr/tutos/tuto-iir1-en.pdf);
- It is also described in [Montgomery, D. C., "Introduction to Statistical Quality Control", page 399, section "The EWMA as a Predictor of Process Level"](https://www.amazon.com/Introduction-Statistical-Quality-Control-Montgomery/dp/1119723094);
- Difficulty adjustment algorithm (DAA) researcher [zawy12](https://github.com/zawy12) regards the DAA family based on EWMA as ["the only difficulty algorithm people should use"](https://github.com/zawy12/difficulty-algorithms/issues/76);
- Bitcoin Cash [DAA](https://read.cash/@jtoomim/bch-upgrade-proposal-use-asert-as-the-new-daa-1d875696) is approximately equivalent to EWMA, and it has [proven itself](https://bitcoincashresearch.org/t/empirical-analysis-of-the-aserts-performance/708) since 2020 when it was activated.

The control function proposed here differs in few ways, in filter terms:

- The input is delayed by 1 sample (x<sub>n-1</sub> instead of x<sub>n</sub>), so that we don't have to recalculate the control function when updating the block template;
- The input is bounded by 0 on the lower end and by the previous sample's output (&epsilon;<sub>n-1</sub>) on the upper end;
- The input is amplified by the asymmetry factor (&zeta;);
- The output's lower end is clipped by the y<sub>0</sub> minimum.

This way the control function will continuously respond to deviations from the "neutral block size" (&epsilon; / &zeta;) and adjust its state (and the new neutral block size) towards the direction of deviation.
The greater the deviation from neutral block size, the greater the response will be.
Because input to the function is bounded, then maximum response will be bounded just as well.

#### Elastic Buffer

The value of the function's output is simply added to the control function's output to obtain the block size limit.
The value of the buffer is calculated using a piecewise function such that:

- the step change on the upside is calculated from the control function's step change but has an additional exponential decay term, and
- on the downside it only has the decay term.

The decay term makes the buffer elastic -- the more it "stretches" relative to the control function, the greater the resistance to further relative increase, and the extreme stretch will be achieved when decay reaches equilibrium with underlying growth rate.

What it means in practice is that if there has been no growth for a long time, then the limit can adapt to a bigger upwards burst and it will more rapidly accommodate that.
However, if there has recently been an explosion of growth, or a steady incline ongoing for a while, then the algorithm will have less "surge capacity" because it already "knows" there has been recent increases.
When network conditions go back to flat again, at a new higher limit, the algorithm will slowly recover the upward surge capacity as the control function grows and elastic buffer decays.

### Choice of Constants

#### Asymmetry Factor

Asymmetry factor determines the relationship between maximum and minimum rates of change of the control curve:

- r<sub>max</sub> = ((&epsilon;<sub>n</sub> - &epsilon;<sub>n-1</sub>) / &epsilon;<sub>n-1</sub>)<sub>max</sub> = &gamma; &sdot; (&zeta; - 1) ,
- r<sub>min</sub> = ((&epsilon;<sub>n</sub> - &epsilon;<sub>n-1</sub>) / &epsilon;<sub>n-1</sub>)<sub>min</sub> = -&gamma; ,
- r<sub>max</sub> / r<sub>min</sub> = 1 - &zeta; ,

and the step response scales down linearly with actual block size and towards the neutral block size (as illustrated in figure in section above).

We can disqualify any &zeta; <= 1 since it would break the purpose of the control function, because it would be impossible to adjust the control function upwards.

The extreme rates would be equal for &zeta; = 2 and the responses to any deviation from the neutral block size would be symmetric.
That would mean that having 50% of the blocks 100% full and the other 50% blocks empty would result in no change on average.
Any value above 2 would give the advantage to full blocks, and enable a minority hash-rate to unilaterally drive an increase, albeit at slowed down rates.
Any value below 2 would give the advantage to empty blocks, and enable a minority hash-rate to unilaterally drive a decrease, albeit at slowed down rates.

Is 2 ideal, then?
This is not desirable, because in the extreme case, that of an adversary obtaining 50% hash-rate and stuffing their blocks with spam to mine 100% full blocks (but not attempting to reorg the other 50%), the "defense" side would have to mine empty blocks and they would miss out on the fee revenue from non-spam transactions.
We do not want the "defense" to be so penalized in the spam scenario.
This narrows the choice to a value between 1 and 2.

For values below 2 we can calculate a "50% defense block size", a block size at which 50% could mine while still preventing the other 50% mining 100% full blocks from moving the control block size.
We can find the value by expressing the defense size as x = k &sdot; &epsilon; and solving for k such that there would be no increase after some N blocks.
Increase after any N blocks would be given by:

- (1 + &gamma; &sdot; (&zeta; - 1))<sup>N/2</sup> (1 + &gamma; &sdot; (&zeta; &sdot; k - 1))<sup>N/2</sup> .

If we solve for no increase, we get:

- k = (&gamma; (&zeta; - 1) - &zeta; + 2) / (&gamma; (&zeta; - 1) &zeta; + &zeta;) .

For small &gamma; this can be approximated with:

- k = (2 - &zeta;) / &zeta;

We propose &zeta; = 1.5 so that tipping point k is approximately 33%.
This way:

- Both the above 50:50 scenario and everyone mining at k = 67% will be equivalent in resulting in no increase of the control function;
- 50% hash-rate mining full blocks can not force the control function to grow;
- In the scenario of a spam attack by 50% hash-rate, the other 50% hash-rate playing defense can still fill their blocks up to k = 33% without resulting in control curve growing.

However, note that:

- 50% hash-rate could force the control function to slowly decay by mining blocks smaller than 33% &sdot; &epsilon; ;
- 33% hash-rate mining empty blocks could counter 67% hash-rate mining full blocks.

We consider this a safer scenario because not including fee-paying transactions results in suboptimal mining and incurs a monetary cost in missed fee revenue, while stuffing miner's own dummy transactions on top of those collected from the mempool would incur only reorg risks.
Also, should some hash-rate attempt to choke the network by mining too small blocks, a growing mempool would provide incentive for more hash-rate to join the "defense" side.

In the case of a 51% attack, the entity controlling the network could produce 100% full or empty blocks all the time, but it could still only affect the control function at rates within boundaries given by the r<sub>min</sub> and r<sub>max</sub> .
After the attack would be thwarted, the control function would return to some equilibrium and undo the attack's effect.

#### Forget Factor

With asymmetry factor (&zeta;) fixed, the forget factor (&gamma;) will determine the maximum and minimum possible year-over-year increase or decrease of the control block size.
We will choose &gamma; such that maximum possible year-over-year (52595 blocks interval) increase will be limited to +100%.

We obtain the value by solving:

- (1 + r<sub>max</sub>)<sup>52595</sup> = 1 + 100% ,

which gives us:

- r<sub>max</sub> = (1 + 100%)<sup>1 / 52595</sup> - 1 = 1 / 75876 , and

- &gamma; = r<sub>max</sub> / (&zeta; - 1) = 1 / 37938 .


**Rationale**

The block size limit should never exceed technological capabilities of the network else it could lead to network collapse or capture by centralized mining pools.
The original [BIP-101](https://github.com/bitcoin/bips/blob/master/bip-0101.mediawiki) was proposing to set the limit to match estimated growth trends of technological capabilities:

>The doubling interval was chosen based on long-term growth trends for CPU power, storage, and Internet bandwidth. The 20-year limit was chosen because exponential growth cannot continue forever. If long-term trends do not continue, maximum block sizes can be reduced by miner consensus (a soft-fork).

While the proposed algorithm's maximum rate is twice the BIP-101, it is [nearly impossible that the algorithm's limit will ever exceed the absolutely-scheduled BIP-101 curve](#algorithm-too-fast), because:

- BIP-101 would have the limit at 128 MB in '24, while the proposed algorithm will be initialized with 32 MB, making it 2 doublings behind;
- Theoretical maximum network load, that of 100% full blocks 100% of the time, would have to persist for more than 1 year in order to intercept the BIP-101 curve;
- Should the network load be only slightly lower, that of 90% full blocks 90% of the time, it would have to persist for about 4.5 years to intercept the BIP-101 curve;
- Any period of lower network load activity extends the runway, the time it would take to catch up with the absolutely-scheduled BIP-101 curve.

#### Elastic Buffer Gearing Ratio and Decay Rate

The gearing ratio (&delta;) determines how fast the buffer may grow, while the decay rate (&theta;) determines how long a relative size may persist during periods of lower activity.
Extreme buffer size relative to control block size is determined from both constants, and given by:

- (&beta;<sub>n</sub> / &epsilon;<sub>n</sub>)<sub>max</sub> = &gamma; / &theta; &sdot; (&zeta; - 1) &sdot; &delta; .

For a set extreme buffer size, choice of the two constants will be coupled and will determine whether buffer is:

- faster to grow and faster to decay, or
- slower to grow and slower to decay.

We propose 3.33 as the relative buffer size, which would let the network "borrow" about a year of block size limit growth (e.g. a quick 2x over the course of a few months), which should be enough to absorb seasonal increases in activity without allowing the limit to stretch too far away from the underlying control block size curve.

We propose a decay rate with half-life of half a year:

- &theta; = 1 / 37938 ,

such that the buffer will then be able to "repay" the borrowed growth in about a year as well, and have a smoothing effect on the underlying control function: as the buffer decreases the control blocksize can continue increasing, to establish a new base for the next wave.

With both the relative size and decay rate fixed, the gearing ratio is calculated as:

- &delta; = (&beta;<sub>n</sub> / &epsilon;<sub>n</sub>)<sub>max</sub> &sdot; (&gamma; / &theta; &sdot; (&zeta; - 1) + 1) / (&gamma; / &theta; &sdot; (&zeta; - 1))  = 10 .

With these constants and if the buffer is empty, it is possible for the network to quickly double the limit, since initial growth rate of (control block size + elastic buffer size) could be r<sub>max</sub> &sdot; (1 + &delta;) because the decay term of the function would be 0.

#### Initial State

The proposed initial state of:

- &epsilon;<sub>0</sub> = 16000000 and
- &beta;<sub>0</sub> = 16000000

will ensure smooth transition from the current flat 32 MB limit and maintain it as the floor for the blocksize limit.

However, any set of constants where &epsilon;<sub>0</sub> + &beta;<sub>0</sub> would equal 32 MB would be a satisfactory initialization, so why this particular set?

By initializing the elastic buffer at stretch ratio (&beta;<sub>0</sub> / &epsilon;<sub>0</sub>) of 1, we won't be accidentally allowing too easy initial gains on the account of expanding the elastic buffer with 0 resistance, as would be the case if initialized with &beta;<sub>0</sub> = 0.

### Backtesting

- Please see [here](backtesting/).

### Simulated Scenarios Testing

- Please see [here](simulations/).

## Specification

The block height which has the activation median time past (MTP) shall be the last block to have the block size limit set to the existing flat block size limit of 32 MB, also referred to as excessive block size (EB) in node software.

The block size limit for the next block(s) shall be auto-adjusted using the [above specified algorithm](#technical-description) and [configuration constants](#configuration-constants), where the excessive blocksize limit (EB) of 32 MB shall serve as the algorithm's floor value.

Please see [reference implementation (C++)](#reference-implementation-c-1) for exact calculation steps and note that **all arithmetic operations must be integer only and carried out in exact same order**.

### Backwards Compatibility

Once deployed, the algorithm will be automatically changing the blocksize limit parameter for nodes that implement and activate it.

Because the nature of the blocksize limit rule is such that it enforces a `<=` condition on block sizes accepted  by nodes, it is possible that nodes without an algorithm, but with a higher configured "flat" limit, will remain in consensus with nodes running with the algorithm -- until the algorithm catches up and a block that actually tests the discrepancy is mined and accepted by nodes running with the algorithm.

Failure to either update the flat limit configuration on time or delegate it to the algorithm would mean that a single block accepted by rest of the network could kick the flat node off the main network.

In order to minimize risk to their users and of network splits, it is strongly advised that all nodes implement the exact same algorithm with exact same parameters and initialize it at exact same time.

### Interaction With Future Fast-sync Schemes

This proposal adds additional requirements for any future trustless node fast-sync scheme: in addition to cryptographic commitment of the UTXO snapshot, a hash of historical blocksizes would need to be committed and verified as well.
Fast-syncing pruned node clients would download both the UTXO snapshot and historical blocksizes from their peers, verify them against the commitments, and use the blocksizes to determine current blocksize algorithm's state and maximum blocksize limit for new blocks.
Note that current size of historical blocksizes dataset is about 7 MB, which is trivial compared to the size of UTXO snapshots which would be multiple gigabytes.

[[Back to Contents](#contents)]

## Security Analysis

[[Back to Contents](#contents)]

The algorithm's implementation relies on simple data structures, conditional branches and integer arithmetic operations which are natively supported by 64-bit CPU architecture.
Therefore, we posit that it doesn't introduce any new DoS vectors or vulnerabilities.

However, the upgrade allows theoretically unbounded growth of the blocksize limit, which could expose network participants to some existing but currently unknown vulnerabilities.
Such a scenario [actually occurred](https://monero.stackexchange.com/questions/421/what-happened-at-block-202612) on Monero (XMR) network:

>The long and the short of it is: the tree-hash.c code that is responsible for transaction hashes within a block had a bug in it whereby any hashes for more than 512 transactions would be computed using uninitialised memory (ie. with garbage). So the attacker slowly built up the dynamic block size limit until they were able to create a 72kb block with 514 transactions in it. Because the hash on transaction 513 and 514 in that block was computed using some random bits of memory on the mining pool's node at the time, a fork occurred. The network eventually chose one of the forks.

Difference from Bitcoin Cash network is in that Monero network was young at the time and nobody actually tested node software with bigger blocks ahead of the algorithm actually reaching the problematic size, so bigger sizes were effectively being tested in production.

With ["scalenet"](https://docs.bitcoincashnode.org/doc/test-networks/#scalenet) testnet, Bitcoin Cash already has a mature testing framework.
On scalenet, 256 MB blocks have been [extensively tested](https://bitcoincashresearch.org/t/assessing-the-scaling-performance-of-several-categories-of-bch-network-software/754) not just with node software, but with other classes of critical infrastructure software (indexer software, pool software, light wallet back-end software).

Even in most optimistic adoption scenarios, it would likely take years for the proposed algorithm to reach a state where it would allow block sizes beyond 256 MB.
By the time the network gets to that state, we expect scalenet tests to stay ahead of the curve and already be testing blocks of 512 MB or 1 GB and beyond.

## Implementations

[[Back to Contents](#contents)]

### Node Implementations

- Bitcoin Cash Node (BCHN): [Merge Request 1782 Implement CHIP-2023-04 Adaptive Blocksize Limit Algorithm](https://gitlab.com/bitcoin-cash-node/bitcoin-cash-node/-/merge_requests/1782)

### Reference Implementation (C)

- Please see [here](implementation-c/src/abla-ewma-elastic-buffer.c#L109).

### Reference Implementation (C++)

- Please see [here](implementation-cpp/src/abla-ewma-elastic-buffer.cpp#L162).

### Spreadsheet Implementation (ODS)

This is implemented using floating-point arithmetics, and intended **for information only.** 

- Please see [here](implementation-ods/src/abla-ewma-elastic-buffer.ods).

Floating-point arithmetics MUST NOT be used by consensus-sensitive application since they are guaranteed to start diverging from integer-only implementations even when using double floating-point precision and before the limit reaching maximum safe integer value of floating point numbers ([2<sup>53</sup>](https://stackoverflow.com/questions/1848700/biggest-integer-that-can-be-stored-in-a-double)).
This is due to the &zeta; &sdot; &beta;<sub>n-1</sub> &sdot; (&zeta; &sdot; x<sub>n-1</sub> - &epsilon;<sub>n-1</sub>) / (&zeta; &sdot; y<sub>n-1</sub> - &epsilon;<sub>n-1</sub>)) term, where floating-point arithmetics can cache the intermediate multiplication only with 53 bits of precision in the mantissa whereas integer muldiv will preserve full 128 bits of precision before performing the division.

## Test Cases

[[Back to Contents](#contents)]

- Please see [here](test-vectors/).

## Activation Costs

[[Back to Contents](#contents)]

We define activation cost as any cost that would arise ex-ante, in anticipation of the activation by affected stakeholders, to ensure that their services will continue to function without interruption once the activation block has been mined.
In case of this proposal, activation cost is contained to nodes and will amount to:

- Some fixed amount of node developer man-hours, in order to release updated node software that will be able to correctly calculate the algorithmic limit.
- Some fixed amount of work by stakeholders running node software, in order to update the software in anticipation of algorithm activation.
- Some fixed amount of effort by others involved in reviewing and testing the new node software version.

This is the same kind of cost that any network upgrade has had or will have.
Nothing is required of stakeholders who are neither running nodes nor involved in node development.

## Ongoing Costs

[[Back to Contents](#contents)]

We define ongoing cost as any cost that would arise post upgrade.

This upgrade is a conceptually simple change but the consequence of the change is such that, in theory, node operating costs will become unbounded.

However, the key aspect of the proposal is that costs will not be uncontrollable, because:

- The maximum rate of change of the limit is bounded by the chosen constants: 2x / year rate for the control function in the extreme edge case of 100% full blocks 100% of the time;
- Actual rate of change will be controlled by network participants: miners who accept to mine blocks that would affect the limit, and users who would produce transactions in sufficient volume to fill those blocks enough to affect the limit;
- Support of more than 50% hashrate will be required to continue growing the control function;
- The limit will never be too far from whatever is the current network throughput, meaning it will still protect the network against shocks or DoS attempts;
- Minority hashrate [can not force the control curve to grow indefinitely](#spam-attack), even if stuffing blocks with their own transactions to consistently mine 100% full blocks;
- Mining bigger blocks has its own costs in increased propagation time and reorg risk, so increasing costs on network infrastructure does not come for free to whomever is producing the blocks;
- The algorithm is capable of *decreasing* the cost by adjusting the limit downwards, if there is not enough transaction load;
- Finally, changing to a slower algorithm or reverting to a flat limit will always be available as fallback, and the proposed algorithm's rate limit means the network will have sufficient time to coordinate it, should there be a need.

## Risk Assessment

[[Back to Contents](#contents)]

If we define risk as [probability times severity](https://en.wikipedia.org/wiki/Risk#Expected_values) then we could make an estimate using the [risk matrix](https://en.wikipedia.org/wiki/Risk_matrix).

Generic consensus upgrade risks apply.
Probability of bugs and network splits is entirely controlled by node developers and quality of their work.
After the node software QC process is completed, the probability of a bug is expected to be very low.

### Algorithm Too Fast

Even in most optimistic organic growth scenarios it is very unlikely that network conditions would become such to push the limit rates close to the algorithm's extremes, as it would take unprecedented levels of activity to reach those rates.

The algorithm's maximum per-block-rate of change has been carefully chosen so that it could not practically exceed the original BIP-101 schedule, which is still seen as a good prediction of feasible network capacity such that it would not lead to mining centralization due to increasing orphan rates.

To illustrate, even with 90% blocks full 90% of the time (Scenario 14 in figure below), it would take about 4.5 years to intercept the original BIP-101 curve (plotted as "upperBound").

Even with periods of 100% blocks full 100% of the time, some dips in network load would be sufficient to slow it down enough to never intercept the BIP-101 curve.

| Scenario 13 |Scenario 14 |
| -- | -- |
| Cycling:<br/>- 100% blocks 100% full for 8 months, then <br/>- 33% blocks 100% full and other 67% at 21 MB for 4 months  | Steady-state of 90% full blocks 90% of the time and other 10% at 21 MB |
| <img src=simulations/results/abla-ewma-elastic-buffer-bounded-01-ne-scenario-13.png width=300></img> | <img src=simulations/results/abla-ewma-elastic-buffer-bounded-01-ne-scenario-14.png width=300></img> |

Even if some metaphorical adoption switch would be flipped and transaction baseload volume would jump overnight, network participants would have ample time to react if the network infrastructure would not be ready, meaning mitigations of this risk are possible:

- miners petitioned to adjust their block size self-limit or fee policy (low lead time),
- network-wide coordinated adjustment of the minimum relay fee or network-wide coordinated changing to a slower algorithm or reverting to a flat limit (high lead time).

During response lead time, the algorithm could work the limit higher.
However, maximum growth rate of the algorithm is such that even response lead time of 1 year would be tolerable.

### Spam Attack

Primary defense against spam is the minimum relay fee, which can be thought of as the minimum economic value of a transaction above which the transaction should not be considered spam but rather regular and rational network use.
However, the relay fee is a network policy which can be circumvented by anyone with access to hashrate.
Having access to hashrate would allow a spammer to have 0-fee transactions mined.

In that scenario, of some hashrate stuffing their own blocks with spam transactions, the impact would be very limited even if the spammer had access to 50% hashrate (Scenario 15 in figure below).
It would take him 1.5 years to have the algorithm double the block size limit, and 2.5 more years for another +50%, meaning costs would keep increasing while the "gains" would be giving diminishing returns.

Such an "attack" would fail to achieve anything, and, when the spam would stop, the algorithm would work the limit back down to whatever is supported by baseload of regular transactions, undoing the spammer's gains.
During the entire time, the spammer would be paying increasing cost of attack in the form of increased orphan risks, while the "defense" would just be mining 21 MB blocks and continue to earn fees from regular transactions.
Once equilibrium is reached, limit could be further increased only if the other 50% would start mining bigger blocks, and the limit would find a new equilibrium.

Should the attacker have "only" 33% hashrate (Scenario 16 in figure below), he wouldn't even be able to double the limit since the equilibrium would be reached at a lower multiple.

| Scenario 15 |Scenario 16 |
| -- | -- |
| - Years 1-4: 50% blocks 100% full, other 50% at 10.67 MB;<br/>- Years 5-8: 50% blocks 100% full, other 50% at 21.33 MB. | - Years 1-4: 33% blocks 100% full, other 67% at 10.67 MB;<br/>- Years 5-8: 33% blocks 100% full, other 50% at 21.33 MB. |
| <img src=simulations/results/abla-ewma-elastic-buffer-01-scenario-15.png width=300></img> | <img src=simulations/results/abla-ewma-elastic-buffer-01-scenario-16.png width=300></img> |

### Algorithm Too Slow

Back-testing the algorithm against hypothetical combined load of 4 major blockchains (BTC + LTC + ETH + BCH), initialized with 1 MB in 2009, shows us that generally it would have been able to accommodate historical growth, without the limit pressing against organic adoption.

| Combined block size of BTC, LTC, ETH, and BCH |
| -- |
| <img src=backtesting/results/abla-ewma-elastic-buffer-01-hybrid-btc+ltc+eth+bch.png width=300></img> |

The algorithm is designed to maintain the limit some level above current transaction baseload and track the general trend while still retaining ability to be responsive to moderate burst load.
The elastic buffer will give the algorithm capability to respond faster to a moderate burst load, however, the "stretch" from the slower-moving control curve and stretch rates are still rate-limited.
This means that the limit could still be hit on occasion, but higher frequency of hits would only be temporary.

| Scenario 05 |
| -- |
| - Year 1: 80% blocks 80% full, other 20% at 12 MB;<br/>- Years 2-4: 50% blocks at 24 MB, other 50% at 16 MB;<br/>- Year 5: 80% blocks 80% full, other 20% at 12 MB;<br/>- Years 6-8: 50% blocks at 48 MB, other 50% at 32 MB. |
| <img src=simulations/results/abla-ewma-elastic-buffer-01-scenario-05.png width=300></img> |

For Bitcoin Cash (BCH) network, we propose to initialize the algorithm with 32 MB minimum since that is the currently accepted limit, which is a much higher base meaning it would take a much higher volume of transactions to achieve comparable growth-rates.
With this in mind, it is unlikely that the algorithm would ever be too slow, and the consequence of a sudden increase in baseload would have only temporary and limited effect to available headroom, because it would catch up after some time.

Compared to current state of having a flat limit which requires manual adjustment and coordination, the algorithm reduces the risk of the limit being inadequate, since coordinated adjustment of a flat limit has lead time measured in months, if not years, whereas the algorithm will start responding immediately when load arrives.

## Evaluation of Alternatives

[[Back to Contents](#contents)]

### No Increase

With no increase, the network will continue to simply work and grow unhampered until mined blocksizes would start getting close to the limit.
If adoption trajectory of the Ethereum network is any indication (it recently peaked at approximately 9 MB every 10 minutes), then the current 32 MB limit could remain adequate for at least a few years.
This means that, at current time, there is no direct opportunity cost for the network, because current network utilization isn't anywhere close to the limit, so there is currently no urgency in increasing the limit.

However, there is a growing "meta" opportunity cost.
Will it become harder to manage an increase later, when utilization actually starts getting close to the limit?
It is unknowable how the future set of network participants and stakeholders would respond to block sizes approaching the limit.
The first Bitcoin network failed to increase it on time, because the "meta cost" of smoothly increasing it was already too high.
How can we ensure that, by the time utilization gets close to the limit, the network participants will manage to increase it on time?
With whatever flat limit in place, it will take a coordinated action by network participants to move it to the next value.
With time, the number of network participants is expected to grow, making coordination and agreement harder.
If the meta cost of coordination grows, then the risk of getting deadlocked again grows, too.
The risk of future deadlock will be present as long as "doing nothing" means that the current limit will stay in place and a coordinated "doing something" would be needed to change the limit.

This proposal would change the "meta game" so that "doing nothing" means the network can still continue to grow in response to utilization, while "doing something" would be required to prevent the network from growing.
The "meta cost" would have to be paid to hamper growth, instead of having to be paid to allow growth to continue, making the network more resistant to social capture.

### One-time Increase

[Recent tests on scalenet](https://bitcoincashresearch.org/t/754) and the fact that a [Raspberry Pi](https://read.cash/@mtrycz/how-my-rpi4-handles-scalenets-256mb-blocks-e356213b) is capable of processing 256 MB blocks suggest that the network infrastructure is likely already capable of handling 256 MB blocks.
Question is, should the limit be raised even though there is not yet demand for even 1 MB blocks?
[In the section above](#no-increase) we already established that there's no urgency or direct benefit in increasing the limit.
The only benefit of increasing the limit to 256 MB right now would be a "meta benefit": sending a signal that our network is ready for more, even if it is not actually being used more.
But what would the costs be?

The original 1 MB limit was called "anti-spam" for a good reason.
Satoshi Nakamoto [argued](https://www.bitcoin.com/satoshi-archive/emails/mike-hearn/9/#selection-25.4677-25.5368) that the limit should be phased in once actual use gets close to it (context was mining node's default self-limit of 500 kB, the 1 MB limit came later):

>A higher limit can be phased in once we have actual use closer to the limit and make sure it's working OK.
>
>Eventually when we have client-only implementations, the block chain size won't matter much.  Until then, while all users still have to download the entire block chain to start, it's nice if we can keep it down to a reasonable size.
>
>With very high transaction volume, network nodes would consolidate and there would be more pooled mining and GPU farms, and users would run client-only.  With dev work on optimizing and parallelizing, it can keep scaling up.
>
>Whatever the current capacity of the software is, it automatically grows at the rate of Moore's Law, about 60% per year.

When block capacity is underutilized then the opportunity cost of mining "spam" is less than when blocks are more utilized.
We will here define "spam" as transactions of extremely low economic value, but how do we establish value when value is subjective?
The fee paid is a good indicator of economic value: if someone is willing to pay 1 satoshi / byte in fees, then it is a proof that the transaction has economic value (to that someone, subjective value) greater than the fee paid.
Consider the current state of the network: the limit is 32 MB while only a few 100 kBs are actually used.
The current network relay minimum fee is 1 satoshi / byte, but some mining pool could ignore it and allow someone to fill the rest with 31.8 MB of 0 fee or heavily discounted transactions.
The pool would only have increased reorg risk, while the entire network would have to bear the cost of processing these transactions.

The limit can therefore also be thought of as minimum hardware requirements, or minimum cost of participation in the network.
Some selfish pool could increase the cost of participation for everyone - much before the network reaches "escape velocity" of utilization, and in doing so reduce chances of network success by increasing adoption friction.

What do we mean by "escape velocity"? It would be a situation where many businesses would be running their own infrastructure in order to securely participate in a high-value blockchain network, and it would just be a business cost easily offset by their earnings.
If the network would succeed to attract, say, 20 MB worth of economic utility, then it is expected that a larger number of network participants would have enough economic capacity to bear the infrastructure costs.
Also, if there was consistent demand of 1 satoshi / byte transactions, e.g. such that they would be enough to fill 20 MB blocks, then there would only be room for 12 MB worth of "spam", and a pool choosing 0 fee over 1 satoshi / byte transactions would have an opportunity cost in addition to reorg risk.

Ideally, the limit would float at some distance above what's currently used, and move only when actual use starts getting close to it.
This would limit the impact of anyone trying to abuse the available "free" space, since the amount of free space relative to economically utilized space would be lower.

Also, a one-time increase doesn't solve the problem of "meta cost" having to be paid again and again -- it would have to be paid every time the network gets close to the limit, and it would be a greater and greater cost every time, increasing the risk of getting stuck on whatever limit was last set.

This proposal addresses both problems: the "meta cost" will have to be paid only once, and the limit would automatically be moved as the network grows.
The proposed algorithm is [resistant to spam scenarios](#spam-attack) because more than 50% hash-rate will have to be in agreement to continuously move the limit.
Some pool with 10% hash-rate could only stretch the elastic buffer by mining 100% full blocks, but it couldn't move the algorithm's control function baseline.
The proposal also achieves the "meta benefit": signalling to everyone that the network is committed to adjusting the limit in response to growth.

### No Limit

The benefit of entirely removing the limit would be in removing the "meta cost" associated with coordinating increases, but it would ruin the public image of the network since it would be a rather irresponsible change.

The cost of this approach would be even higher than the one-time increase, as it would increase unpredictability of the network, and expose it to even more extreme scenarios of the "free" space being abused.

This approach has actually been tested by Bitcoin SV, and a [comment](https://github.com/Blockchair/Blockchair.Support/issues/910#issuecomment-1159369293) by Nikita Zhavoronkov (lead developer of Blockchair block explorer) illustrates the problem well:

>I think the main issue with BSV in general is that it's not quite possible to predict the blockchain size at all. The current limit seems to be 4 GB and people were actively testing to hit it. So potentially it's 4 * 144 = 576 GB of blockchain data every day. Plus indexes. Plus services like block explorers run their own database (we run even two for extra speed and analytics). So for Blockchair this is potentially up to 60 terabytes a month just with the current limit (which is expected to get increased).
>
>The second important issue is that if it was some useful data like real transactions, real people would come to block explorers to see their transactions, businesses would buy API subscriptions, so we'd be able to cover the disk costs, the development costs, the cost of trying to figure our how to fit 10 exabytes into Postgres (not very trivial I think), etc.
>
>But the reality is that 99.99% or so of Bitcoin SV transactions are junk, so despite being the biggest Bitcoin-like blockchain with most transactions, Bitcoin SV constitutes only 0.3% of our visitor numbers and there are very few API clients using Bitcoin SV (0.2% of all API requests most of which are free API calls for the stats). Unfortunately, this doesn't cover all these costs. So that's why we can't run more than 2 nodes, and even these two nodes will get stuck at some point because we'll go bankrupt buying all these disks to store the junk data. But we're trying our best :)
>
>With this amount of junk data I just don't see a business model for a BSV explorer which would work in the long term (maybe an explorer run by a miner?). The same goes for exchanges for example I think. If you have to buy 10 racks of servers to validate the blockchain, but you only have 10 clients paying trading fees, you'll go bankrupt.

With the proposed algorithm, we can get the benefit of removing the "meta cost", while also avoiding the extreme scenario like the one above.

### Absolutely Scheduled

Past proposals like [BIP-101](https://github.com/bitcoin/bips/blob/master/bip-0101.mediawiki) and [BIP-103](https://github.com/bitcoin/bips/blob/master/bip-0103.mediawiki) have proposed an absolute schedule of increasing the limit, with BIP-101 proposing increases of +100% every 2 years, and BIP-103 proposing +4.4% every 97 days (about +17.7% every year).
Both have the same issues:

- Risk of providing too much headroom ahead of time, in case expected utilization just doesn't happen as anticipated.
This is a problem for the same reason as too early [one-time increase](#one-time-increase) would be.
- Inability to adjust downwards during periods of underutilization.
- Inability to absorb waves of utilization coming in bursts, if they would happen ahead of schedule.

This proposal can match or exceed the rates of increase of both proposals, but only if the network's actual utilization would be maintained such to drive the algorithm at those rates.

### Algorithmic

#### Based on Past Blocksizes

This proposal itself belongs in this category, as it only needs historical mined blocksizes to determine the current limit.

We have considered 2 other proposals using the same general approach, but using the median function instead:

- Single median, [originally proposed by Stephen Pair and Chris Kleeschulte](https://github.com/bitpay/bips/blob/master/bip-adaptiveblocksize.mediawiki);
- Dual median, [originally proposed by imaginary_username](https://bitcoincashresearch.org/t/asymmetric-moving-maxblocksize-based-on-median/197).

It should be noted that the dual median algorithm has actually been [deployed in production](https://jqrgen.medium.com/adoptive-block-size-of-nexa-ee1e058f73ef) on a new blockchain network called "Nexa", a project launched by Bitcoin Unlimited in June 2022, which can now serve as validation of the general approach.

The main rationale for the median-based approach has been resistance to being disproportionately influenced by minority hash-rate:

>By having a maximum block size that adjusts based on the median block size of the past blocks, the degree to which a single miner can influence the decision over what the maximum block size is directly proportional to their own mining hash rate on the network. The only way a single miner can make a unilateral decision on block size would be if they had greater than 50% of the mining power.

This is indeed a desirable property, which [this proposal preserves](#spam-attack) while improving on other aspects:

- The response is well behaved across the full range of possible scenarios, as opposed to median which can be unstable at edge cases, and in some cases the median won't move at all even as the average blocksize grows;
- The response intensity is smoothly and instantly adjusting to changes instead of having to wait for bigger sizes to enter the median window and with sufficient frequency;
- Headroom ratio (block size limit / average block size) is better maintained across steady-state scenarios.

Instability when hashrate is at the 50% edge can be observed below.

| Scenario 15 - Dual Median |Scenario 15 - Proposed |
| -- | -- |
| - Years 1-4: 50% blocks 100% full, other 50% at 10.67 MB;<br/>- Years 5-8: 50% blocks 100% full, other 50% at 21.33 MB. | - Years 1-4: 50% blocks 100% full, other 50% at 10.67 MB;<br/>- Years 5-8: 50% blocks 100% full, other 50% at 21.33 MB. |
| <img src=simulations/results/abla-dual-median-01-scenario-15.png width=300></img> | <img src=simulations/results/abla-ewma-elastic-buffer-01-scenario-15.png width=300></img> |

Jagged response and lag of the median can be observed below.

| Scenario 12 - Dual Median |Scenario 12 - Proposed |
| -- | -- |
| 1. 10% blocks 100% full, other 90% at 21 MB;<br/>2. 50% blocks 100% full, other 50% at 21 MB.<br/>3. 10% blocks 100% full, other 90% at 32 MB;<br/>4. 50% blocks 100% full, other 50% at 32 MB.<br/>5. 10% blocks 100% full, other 90% at 32 MB. | 1. 10% blocks 100% full, other 90% at 21 MB;<br/>2. 50% blocks 100% full, other 50% at 21 MB.<br/>3. 10% blocks 100% full, other 90% at 32 MB;<br/>4. 50% blocks 100% full, other 50% at 32 MB.<br/>5. 10% blocks 100% full, other 90% at 32 MB. |
| <img src=simulations/results/abla-dual-median-01-scenario-12.png width=300></img> | <img src=simulations/results/abla-ewma-elastic-buffer-01-scenario-12.png width=300></img> |

The headroom ratio (block size limit / average block size) in steady-state scenarios will depend on distribution of sizes for a given average.
The single median proposal by Pair & Kleeschulte was proposing the limit to be 2x the median, which would provide 2x headroom only in the edge case when average is equal to median, and it would be higher or lower for all other cases.

In both below scenarios, averages are equal to 32, 64, and 128 MB. However, in the 1st scenario the average consists of 33% bigger and 67% smaller blocks, while in the 2nd scenario it consists of 66% bigger and 34% smaller blocks.

In the below scenario we can observe that median stabilizes at about **1.5x** headroom ratio, while the proposed algorithm stabilizes at about **2.2x**.

| Scenario 17 - Single Median |Scenario 17 - Proposed |
| -- | -- |
| 1. 33% blocks at 48 MB, other 67% at 24.12 MB;<br/>2. 33% blocks at 96 MB, other 67% at 48.24 MB;<br/>3. 33% blocks at 192 MB, other 67% at 96.48 MB. | 1. 33% blocks at 48 MB, other 67% at 24.12 MB;<br/>2. 33% blocks at 96 MB, other 67% at 48.24 MB;<br/>3. 33% blocks at 192 MB, other 67% at 96.48 MB. |
| <img src=simulations/results/abla-median-01-scenario-17.png width=300></img> | <img src=simulations/results/abla-ewma-elastic-buffer-01-scenario-17.png width=300></img> |

In the below scenario we can observe that median stabilizes at about **2.5x** headroom ratio, while the proposed algorithm stabilizes at about **2.3x**.

| Scenario 18 - Single Median |Scenario 18 - Proposed |
| -- | -- |
| 1. 66% blocks at 40.24 MB, other 34% at 16 MB;<br/>2. 66% blocks at 80.48 MB, other 34% at 32 MB;<br/>3. 66% blocks at 160.96 MB, other 34% at 64 MB. | 1. 66% blocks at 40.24 MB, other 34% at 16 MB;<br/>2. 66% blocks at 80.48 MB, other 34% at 32 MB;<br/>3. 66% blocks at 160.96 MB, other 34% at 64 MB. |
| <img src=simulations/results/abla-median-01-scenario-18.png width=300></img> | <img src=simulations/results/abla-ewma-elastic-buffer-01-scenario-18.png width=300></img> |

#### Based on Other Metrics

A notable proposal in this category was ["Elastic Block Caps"](https://scalingbitcoin.org/transcript/telaviv2019/elastic-block-caps), originally proposed by Meni Rosenfield, with the idea to sample past block fees as input driving the control function.

Problem with that approach is that the algorithm must set an economic policy about how much in fees is required to drive an increase, but the algorithm can't know whether the fees will make sense in some later market context, since market prices are external information.

With our proposal, the fee / byte is instead open to free market negotiation between miners and users, while the limit will simply float at a distance above whatever is actually negotiated, and continuously provide room for future negotiations.
The proposed algorithm is agnostic of the market forces at play that will result in miners accepting to mine bigger blocks, it will simply respond to the fact by providing more room to allow the growth trend to continue.

### Hybrid - Algorithmic With Absolutely Scheduled Guardrails

The idea surfaced during discussions about this proposal ([1](https://old.reddit.com/r/btc/comments/14x27lu/chip202301_excessive_blocksize_adjustment/js0fydn/), [2](https://bitcoincashresearch.org/t/1037/23)), quoting [Jonathan Toomim](https://old.reddit.com/r/btc/comments/14x27lu/chip202301_excessive_blocksize_adjustment/jrvdfaa/):

>My suggestion for a hybrid BIP101+demand algorithm would be a bit different:
>
>1. The block size limit can never be less than a lower bound, which is defined solely in terms of time (or, alternately and mostly equivalently, block height).
>2. The lower bound increases exponentially at a rate of 2x every e.g. 4 years (half BIP101's rate). Using the same constants and formula in BIP101 except the doubling period gives a current value of 55 MB for the lower bound, which seems fairly reasonable (but a bit conservative) to me.
>3. When blocks are full, the limit can increase past the lower bound in response to demand, but the increase is limited to doubling every 1 year (i.e. 0.0013188% increase per block for a 100% full block).
>4. If subsequent blocks are empty, the limit can decrease, but not past the lower bound specified in #2.
>My justification for this is that while demand is not an indicator of capacity, it is able to slowly drive changes in capacity. If demand is consistently high, investment in software upgrades and higher-budget hardware is likely to also be high, and network capacity growth is likely to exceed the constant-cost-hardware-performance curve.

#### Absolutely-scheduled Upper Bound

The algorithm as it is has a built-in upper bound in the maximum per-block increase attainable only with 100% full blocks.
This version of the proposal was revised to [implement the suggested max. rate](#forget-factor), and so the "forget factor" (gamma) is defined above as:

- 1 / &gamma; = 1 / 37938, from which we can calculate
- r<sub>max</sub> = (&zeta; - 1) / &gamma; = 1 / 75876,

which translates to a maximum per-block-increase of 0.0013179397965101% (in case of a 100% full block), and would compound to +100% year-over-year *only if all the blocks* in the year were 100% full.

Because of this, an additional fixed-schedule upper bound curve (such as BIP-101) is not necessary, since the algorithm as it is could not catch up with it in practice, which we demonstrated in [the section above](#algorithm-too-fast).

#### Absolutely-scheduled Lower Bound

The problem with lower bound is that it would be only somewhat useful during bootstrapping the proposed demand-driven algorithm, in that it would be increasing the base from which the demand-driven algorithm would start once demand to drive it would build up, while later it could become effectively no limit if it would race on much ahead of actual usage.

We can illustrate the first point with the below figure, which is backtesting the algorithm against combined volume of BTC + LTC + ETH + BCH, with / without the guardrails enforced, and initialized with 707 kB starting point in 2009.

<img src=img/fig-hybrid.png></img>

We can make the following observations:

- Absolutely-scheduled upper bound curve (BIP-101) quickly races on and becomes practically impossible to catch-up with.
- Absolutely-scheduled lower bound curve (half the BIP-101 rate) at first works to grow the limit even without any volume.
However, once volume comes, the demand-driven limit (green) crosses over, takes over, and doesn’t fall back, making the lower bound useless from the moment demand reaches escape velocity.
- Even without enforcing the lower bound, the network activity itself is able to drive the algorithm to form a monotonic "controlBlockSize" (orange) curve.
The demand-driven curve doesn’t fall down below the general growth trendline, even though it theoretically could.
The “neutralBlockSize” (blue) is the threshold for the algorithm, and as long as more bytes are mined above it than gaps below it -- it all keeps going up.

Note that upper bound here was chosen to match the original BIP-101, from which we worked it backwards to 707 kB at Bitcoin genesis and used as the starting point for all 3 curves.
When initialized as such, the schedule would be bringing the lower bound to 9.5 MB in 2024.
If we’re so much ahead of such schedule with the proposed 32 MB flat floor value, then is a moving floor even necessary?

The 32 MB flat floor (as proposed) is plenty to bootstrap the demand-driven algorithm, considering it could fit 4x the current average volume of all 4 blockchains combined.
The proposed algorithm is designed to work well for demand when it actually arrives, and not cater to speculative demand which can be neither proven nor falsified, which the absolutely-scheduled lower bound would be doing.

Jonathan Toomim was proposing a starting point of 32 MB in 2024 for the lower bound, which would bring the lower bound to 64 MB by 2028.
The problem is that it would require all network participants to over-provision the capacity even more for no reason other than speculative, even if demand would fail to break above 10.67 MB (initial neutral block size) of sustained use by 2028.

Further delay of achieving network utilization would increase "stand-by" costs (or prevent costs savings due to tech. advancements) of general infrastructure (indexers, explorers, etc.) without there being some offsetting benefit in the form of increased network value.
If transaction demand were to come, then the lower bound would be unnecessary since the demand-driven algorithm would take over.

It will likely become reasonable to increase the "stand-by" capacity simply because it will become trivial to support it even if it will be orders of magnitude underutilized.
After all, that is the current state (few 100 kBs utilized of available 32 MB).
However, it is impossible to predict what will be trivial in the absence of network growth, and the argument here is that it should be left to good judgement of network participants, e.g. some future CHIP proposing to lift the floor value to 64 MB in 2028, instead of being delegated to a fixed schedule.

To attempt to propose a fixed-schedule for the floor value now would be scope creep for this CHIP.
This proposal never set out to predict what people will do -- but instead to: react to what they’re actually doing, remove the risk of dead-lock, and count on the incentives of TX load “pushing” people to keep upgrading the network capacity to stay ahead of demand as it comes, while still being slow enough at the extremes that it would be manageable.

### Hash-rate Polling

This category refers to simply asking miners to write messages in support of ideas into coinbase, without there being a code-path in node software to actually act on such messages.
The main problems with this are:

- It costs nothing to cast, withdraw, or change a vote, since pools can anyway write whatever they want into coinbase message, and it will appear at a frequency closely matching their hash-rate.
- It is unpredictable, since pools can change the vote at their whim.
- It actually increases the "meta cost" of coordination, since it creates a new meta game.
- Non-aligned incentives.
Since casting a vote has no additional costs, pools could offer to sell the voting capacity to others.
Mining is anonymous by nature, and pools could impersonate each other just as well.
This opens the door to covert manipulations, and actually worsens the signal-to-noise ratio of coordination efforts.
- Execution is still dependent on nodes giving the votes relevancy by agreeing to act on the results.

Failures of hash-rate polling are evidenced by past attempts, as seen in on-chain records displayed below (chart generated using [nakamoto.observer](https://nakamoto.observer/signals) website):

<img src=img/bitcoin-signals.png width=400></img>

Currently, Bitcoin Cash captures only a small minority of sha256d hashrate, so there are additional issues a hashrate polling system would face:

- Low participation.
- Possibility of buying votes from miners favoring other networks, but interested in selling votes to the highest bidder.

### Hash-rate Direct Voting

This is a variation of the above but where nodes would accept to automatically execute some adjustment in their consensus-sensitive parameters, and in response to coinbase or block header messages.
Notable proposal of this category were [BIP-100](https://github.com/bitcoin/bips/blob/master/bip-0100.mediawiki) and [BIP-0105](https://github.com/bitcoin/bips/blob/master/bip-0105.mediawiki), and the idea resurfaced in a [recent discussion with zawy](https://twitter.com/zawy3/status/1663900762743742466).

The problems are similar to polling, but at least execution would be guaranteed since nodes would be coded to automatically react to votes cast.
It doesn't resolve the issue of "meta cost" as it would require active engagement by network participants in order to have the votes actually be cast.
Alternatively, miners would automate the vote-making decisions by sampling the blockchain in which case the direct voting would become algorithmic adjustment - but with more steps.
The problem is also in knowledge and predictability - other network participants can't know how miners will choose to vote, e.g. 90% full blocks would be no guarantee that miners will instantly choose to make a vote to increase the headroom.

This proposal avoids such problems, because if implemented then everyone would know that everyone knows that the limit WILL move if utilization crosses the algorithm's threshold, and could therefore plan their actions with more certainty.

### Other

Other ideas, such as block reward penalty, would require additional consensus rules that would add complexity, change fundamental properties of the network, and have been disqualified as such.

## Discussions

[[Back to Contents](#contents)]

- [Bitcoin Cash Research (BCR) Forum: This CHIP Discussion](https://bitcoincashresearch.org/t/1037)
- [Bitcoin Cash Research (BCR) Forum: Asymmetric Moving Maxblocksize Based On Median](https://bitcoincashresearch.org/t/asymmetric-moving-maxblocksize-based-on-median/197)
- [Bitcoin Cash Research (BCR) Forum: CHIP 2021-07 UTXO Fastsync](https://bitcoincashresearch.org/t/chip-2021-07-utxo-fastsync/502/21) (discussion about committing historical block sizes in order to support an algorithm in fast-sync/pruned mode)
- [Telegram, Bitcoin Verde: discussion about committing historical block sizes in order to support an algorithm in fast-sync/pruned mode](https://t.me/bitcoinverde/3790)
- [Twitter: Hash-rate Direct Voting VS This Proposal](https://twitter.com/zawy3/status/1663900762743742466)
- [Reddit: This CHIP Discussion #2](https://old.reddit.com/r/btc/comments/15ax3jy/chip202301_adaptive_blocksize_limit_algorithm_for/)
- [Reddit: This CHIP Discussion #1](https://old.reddit.com/r/btc/comments/14x27lu/chip202301_excessive_blocksize_adjustment/)
- [Reddit: Median VS an Older EWMA-based Proposal](https://old.reddit.com/r/btc/comments/10mqi1f/why_dont_cryptocurrencies_such_as_bch_and_xmr/j64mfnv/)
- [Reddit: Manual Adjustment by Individual Nodes VS an Older EWMA-based Proposal](https://old.reddit.com/r/btc/comments/10fz422/scaling_in_a_free_market_and_avoiding_recapture/j54ca2c/)
- [Reddit: Making the Case for an Older EWMA-based Proposal](https://old.reddit.com/r/btc/comments/10fa2vv/bitcoin_cash_bch_developer_bchautist_unveils/)

## Statements

[[Back to Contents](#contents)]

- Please [see here](./stakeholders.md).

## Acknowledgements

[[Back to Contents](#contents)]

## Changelog

[[Back to Contents](#contents)]

Changes are tracked using [git commits](https://gitlab.com/0353F40E/ebaa/-/commits), we will highlight some notable past versions here:

- Current - Add link to BCHN implementation, add testnet parameters, revise notes about the 2 GB temporary limit
- 2023-09-28 - 3f52c56a - Update header & changelog, add internal input clamping for implementation safety, add utility "lookahead" function to reference implementations, add more test vectors.
- 2023-09-02 - 5ced9afe - General improvements to CHIP body, adding stakeholder statements, test vectors, changelog, etc.
Set "hard" internal limits to match proposed configuration.
Additionally, specify a 2 GB temporary ceiling due to 32-bit architecture limitations, which are still supported.
The 2 GB temporary limit is to be deprecated before May 2028.
- 2023-08-06 - a5b1fe12 - Add "hard" ceiling values for control blocksize and elastic buffer size so that intermediate calculation can not overflow uint64 even when growing beyond 100 Petabytes.
- 2023-07-27 - b01a2047 - Fine-tuned configuration constants to increase elastic buffer relative size, and adjusted initialization to start at partially stretched state.
- 2023-07-21 - 0e22abfe - [Replaced secondary "variable multiplier" with "elastic buffer"](https://bitcoincashresearch.org/t/chip-2023-01-excessive-block-size-adjustment-algorithm-ebaa-based-on-weighted-target-exponential-moving-average-wtema-for-bitcoin-cash/1037/35), made the limit be calculated as addition of two functions instead of multiplication. This made it much more elegant and easier to tune. Fine-tuned configuration constants so to make it practically impossible to intercept the original BIP-101 curve.
- 2023-06-13 - 64533c34 - [Replaced step response clipping with linear scaling](https://bitcoincashresearch.org/t/chip-2023-01-excessive-block-size-adjustment-algorithm-ebaa-based-on-weighted-target-exponential-moving-average-wtema-for-bitcoin-cash/1037/16), to have step response be smooth across full range of block fullness.
- 2023-04-13 - db144979 - First attempt at having a secondary function (variable multiplier) to adjust relative headroom. This version was [published on BCR forum](https://bitcoincashresearch.org/t/chip-2023-01-excessive-block-size-adjustment-algorithm-ebaa-based-on-weighted-target-exponential-moving-average-wtema-for-bitcoin-cash/1037).
- 2022-12-28 - 8ef5d63a - Initial version. Simple EWMA with asymmetry factor.

## Copyright

[[Back to Contents](#contents)]

[MIT No Attribution](https://opensource.org/license/mit-0/).
